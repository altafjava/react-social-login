import React, { Component } from 'react';
import PrivateRoute from './components/common/PrivateRoute'
import './App.css';
import { BrowserRouter, Switch, Route } from 'react-router-dom'
import Home from './components/Home/Home'
import SocialLogin from './components/SocialLogin/SocialLogin'
import NotFound from './components/NotFound/NotFound'
import Header from './components/common/Header'
import { getCurrentUser } from './util/APIUtils';
import { ACCESS_TOKEN } from './constants/SocialConstant';
import OAuth2RedirectHandler from './util/OAuth2RedirectHandler'
import Alert from 'react-s-alert';
import Signup from './components/Home/Signup'
import Profile from './components/profile/Profile'
import LoadingIndicator from './components/common/LoadingIndicator'
import Test from './Test'

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      authenticated: false,
      currentUser: null,
      loading: false
    }
  }
  componentDidMount() {
    this.loadCurrentlyLoggedInUser();
  }
  render() {
    if (this.state.loading) {
      return <LoadingIndicator />
    }
    return (
      <div className="app">
        <BrowserRouter>
          <div>
            <div className="app-top-box">
              <Header authenticated={this.state.authenticated} onLogout={this.handleLogout} />
            </div>
            <div className="app-body">
              <Switch>
                <Route exact path="/" component={Home} />
                {console.log('profile')}
                <PrivateRoute path="/profile" authenticated={this.state.authenticated} currentUser={this.state.currentUser}
                  component={Profile}></PrivateRoute>
                <PrivateRoute path="/test" component={Test}></PrivateRoute>
                <Route path="/login"
                  render={(props) => <SocialLogin authenticated={this.state.authenticated} {...props} />}></Route>
                <Route path="/signup"
                  render={(props) => <Signup authenticated={this.state.authenticated} {...props} />}></Route>
                <Route path="/oauth2/redirect" component={OAuth2RedirectHandler}></Route>
                <Route component={NotFound}></Route>
              </Switch>
            </div>
          </div>
        </BrowserRouter>
      </div>
    );
  }
  loadCurrentlyLoggedInUser = () => {
    this.setState({
      loading: true
    });
    getCurrentUser()
      .then(response => {
        this.setState({
          currentUser: response,
          authenticated: true,
          loading: false
        });
      }).catch(error => {
        this.setState({
          loading: false
        });
      });
  }
  handleLogout = () => {
    localStorage.removeItem(ACCESS_TOKEN);
    this.setState({
      authenticated: false,
      currentUser: null
    });
    Alert.success("You're safely logged out!");
  }
}
export default App